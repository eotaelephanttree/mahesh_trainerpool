﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RegistrationMVC.Models;
using Newtonsoft.Json;
using System.IO;
using System.Web.Routing;

namespace RegistrationMVC.Controllers
{

    
    public class AdminController : Controller
    {


         LearningDBEntities emp = new LearningDBEntities();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(Int32 id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                return View(emp.RegTables.Where(x => x.id == id).FirstOrDefault());
            }
        }

        [HttpPost]
        
        public ActionResult Edit(Int32 id, RegTable emp)
        {
             using (Models.LearningDBEntities dbModel = new Models.LearningDBEntities())
                {
                    dbModel.Entry(emp).State = System.Data.Entity.EntityState.Modified;
                    dbModel.SaveChanges();
                    ViewBag.message = " Successful";
                return View();
                    
                    //var db = dbModel.RegTables.Where(z => z.id == id && z.email == emp.email).FirstOrDefault();
                    //if (db == null)
                    //{
                    //    ViewBag.message = "there is no data.";
                    //}
                    //else
                    //{
                    //    dbModel.Entry(emp).State = System.Data.Entity.EntityState.Modified;
                    //    dbModel.SaveChanges();
                    //    ViewBag.message = "successfully edited.";
                    //}


                }
                
          

        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                return View(emp.RegTables.Where(x => x.id == id).FirstOrDefault());
            }
        }
    
        [HttpPost]
        public ActionResult Delete(int id, string st)
        {
            try
            {
                using (Models.LearningDBEntities dbModel = new Models.LearningDBEntities())
                {
                    RegTable emp = dbModel.RegTables.Where(x => x.id == id).FirstOrDefault();
                    dbModel.RegTables.Remove(emp);
                    dbModel.SaveChanges();

                }
                return RedirectToAction("List","Admin");
            }
            catch
            {
                return View();
            }

        }


        [HttpGet]
        public ActionResult List()
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                return View(emp.RegTables.ToList());
            }

        }


        // CATEGORY SECTION....................
        [Authorize]
        public ActionResult ViewCategory(string id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                ViewBag.category = emp.Categories.OrderByDescending(a => a.catId).ToList();
                if (id != null)
                {
                    int categoryId = int.Parse(id);

                    Category model = emp.Categories.Where(x => x.catId == categoryId).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }

            }
        }

        [HttpPost]
        public ActionResult AddCategory(Category catModel, FormCollection testcollection)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {
                if (dbModel.Categories.Any(x => x.name == catModel.name))
                {
                    ViewBag.DuplicateMessage = "Category already Exists";
                    return View("ViewCategory", catModel);
                }

                else
                {
                    catModel.name = testcollection["catname"];
                    catModel.createdDate = DateTime.Now;
                    catModel.modifiedDate = DateTime.Now;
                    if (testcollection["isactive"] == "on")
                    {
                        catModel.isActive = true;
                    }
                    else
                    {
                        catModel.isActive = false;
                    }


                    dbModel.Categories.Add(catModel);
                    dbModel.SaveChanges();
                    ModelState.Clear();
                    ViewBag.SuccessMessage = "Added Successful";
                    //return View("AddorEdit", new RegTable()); //for refreshing or showing new register page
                    return RedirectToAction("ViewCategory", "Admin");
                }

            }
        }


        public ActionResult DeleteCategory(string id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                ViewBag.cat = emp.Categories.OrderByDescending(a => a.catId).ToList();
                if (id != null)
                {
                    int catid = int.Parse(id);

                    Category model = emp.Categories.Where(x => x.catId == catid).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }

            }
        }



        [HttpPost]
        public ActionResult DeleteCategory(string id, Category categoryTable)
        {

            using (Models.LearningDBEntities dbModel = new Models.LearningDBEntities())
            {

                Category emp = dbModel.Categories.Where(x => x.catId == categoryTable.catId).FirstOrDefault();

                if (emp != null)
                {
                    dbModel.Categories.Remove(emp);
                    dbModel.SaveChanges();


                }

                return RedirectToAction("ViewCategory", "Admin");

            }


        }

        [HttpPost]
        public ActionResult SaveCategory(Category categoryModel, string chk, FormCollection frm)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {
                //if(chk == null)
                //{

                //}else
                //{
                //    courseModel.isActive = true;

                //}
                if (frm["chk"] == null)
                {
                    categoryModel.isActive = false;
                }
                else
                {
                    categoryModel.isActive = true;
                }
                categoryModel.modifiedDate = DateTime.Now;
                dbModel.Entry(categoryModel).State = System.Data.Entity.EntityState.Modified;
                dbModel.SaveChanges();
                ViewBag.message = " Successful";
                return RedirectToAction("ViewCategory", "Admin");

                //var db = dbModel.RegTables.Where(z => z.id == id && z.email == emp.email).FirstOrDefault();
                //if (db == null)
                //{
                //    ViewBag.message = "there is no data.";
                //}
                //else
                //{
                //    dbModel.Entry(emp).State = System.Data.Entity.EntityState.Modified;
                //    dbModel.SaveChanges();
                //    ViewBag.message = "successfully edited.";
                //}


            }



        }





        //---------------------- end of CATEGORY SECTION-------------------









        [Authorize]
        public ActionResult MyProfile(RegTable regModal)
        {
            LearningDBEntities db = new LearningDBEntities();
            ViewBag.name = (from a in db.userTables
                            where a.email == User.Identity.Name
                            select a.firstName).Distinct().FirstOrDefault();
            return View();


                    
        }

        // ORGANIZATION SECTION------------
        [Authorize]
        public ActionResult AddOrg()
        {
            
            using (LearningDBEntities org = new LearningDBEntities())
            {
                ViewBag.cat = org.Categories.OrderByDescending(a => a.catId).ToList(); //dropdown for category
                return View();
            }
                

        }


        [HttpPost]
        public ActionResult AddOrg(OrgTable orgModel, userTable userModal, CoorgTable coorgModal, FormCollection testcollection, HttpPostedFileBase image)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {

                if (dbModel.OrgTables.Any(x => x.email == orgModel.email))
            {
                ViewBag.DuplicateMessage = "Organization with this email already exists.";
                return RedirectToAction("AddOrg", "Admin");
            }
            else
            {

                    if (image == null)
                    {
                        userModal.profilePic = null;
                    }
                    else
                    {
                        string imageName = Guid.NewGuid() + Path.GetExtension(image.FileName);
                        image.SaveAs(Path.Combine(Server.MapPath("~/UploadedResume/UploadedImages"), imageName));
                        

                        userModal.profilePic = imageName;

                    }

                    Guid guid = Guid.NewGuid();
                    Random random = new Random();
                    int key = random.Next();

                    orgModel.orgId = key;

                    var category_id = (from a in dbModel.Categories
                                       where a.name == "Organization"
                                       select a.catId).FirstOrDefault();

                    orgModel.catId = category_id;


                    orgModel.orgName = testcollection["orgname"];
                    orgModel.personName = testcollection["first_name"];
                    userModal.lastName = testcollection["last_name"];

                    orgModel.email = testcollection["email"];
                    
                    orgModel.mobile = testcollection["mobile_no"];
                    orgModel.phone = testcollection["phone_no"];
                    orgModel.amount = Convert.ToInt32(testcollection["amount"]);
                    //orgModel.startCourse = Convert.ToDateTime(testcollection["startdate"]);
                    //orgModel.endCourse = Convert.ToDateTime(testcollection["enddate"]);
                    orgModel.createdDate = DateTime.Now;
                    orgModel.modifiedDate = DateTime.Now;
                if (testcollection["isactive"] == "on")
                {
                        orgModel.isActive = true;
                }
                else
                {
                        orgModel.isActive = false;
                }

                dbModel.OrgTables.Add(orgModel);
                dbModel.SaveChanges();
                //ModelState.Clear();
                    // end of adding organization details


                    //start of adding user(Org) details
                    LearningDBEntities ent = new LearningDBEntities();
                    Guid guid1 = Guid.NewGuid();
                    Random random1 = new Random();
                    int key1 = random.Next();

                    //orgModel.orgId = key1;


                    var usr_details = (from a in ent.OrgTables
                                       where a.email == orgModel.email
                                       select new { a.orgId, a.personName, a.email, a.createdDate, a.modifiedDate, a.catId, a.isActive }).Distinct().FirstOrDefault();
                   
                    userModal.userId = key1;
                    userModal.orgId = usr_details.orgId;
                    userModal.firstName = usr_details.personName;
                    userModal.email = usr_details.email;
                    userModal.password = testcollection["pass"];
                    userModal.catId = usr_details.catId;
                    userModal.isActive = usr_details.isActive;

                    //to add role of admin in user table
                    var role = (from a in emp.RoleTables
                                where a.roleName == "Admin"
                                select a.roleId).Distinct().FirstOrDefault();
                    userModal.roleId = role;


                    userModal.createdDate = usr_details.createdDate;
                    userModal.modifiedDate = usr_details.modifiedDate;
                    dbModel.userTables.Add(userModal);
                    dbModel.SaveChanges();
                    ViewBag.SuccessMessage = "Registration Successful";

                    //start of adding CoorgTable details

                    //coorgModal.orgId = key;
                    //coorgModal.

                    

                return RedirectToAction("ViewOrg", "Admin");
            }

        }

    }

        [Authorize]
        public ActionResult EditOrg(int ID)
        {
            using (LearningDBEntities db = new LearningDBEntities())
            {
                
                var user_id = (from a in db.userTables
                               where a.orgId == ID
                               select a.userId).Distinct().FirstOrDefault();
                ViewBag.org = (from a in db.OrgTables
                               where a.orgId == ID
                               select a).Distinct().FirstOrDefault();
                ViewBag.user = (from a in db.userTables
                                where a.userId == user_id
                                select a).Distinct().FirstOrDefault();
                               
            }
                return View();
        }

        [HttpPost]
        public ActionResult SaveOrg(userTable userModal, OrgTable orgModal, FormCollection testcollection, HttpPostedFileBase image)
        {
            using (LearningDBEntities db = new LearningDBEntities())
            {
                orgModal.modifiedDate = DateTime.Now;
               
                var active_state = testcollection["isactive"];
                if(active_state == "on")
                {
                    orgModal.isActive = true;
                }else
                {
                    orgModal.isActive = false;
                }
                
                db.Entry(orgModal).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();


            }

            using (LearningDBEntities db1 = new LearningDBEntities())
            {
                var user_details = (from a in db1.userTables
                                    where a.userId == userModal.userId
                                    select a).Distinct().FirstOrDefault();


                //for editing profile pics

                if (image == null) // because need to retain the previous image.
                {
                    var pic = user_details.profilePic;

                    user_details.profilePic = pic;
                }
                if (image != null)
                {
                    var pic_name=user_details.profilePic;
                    var path = Path.Combine(Server.MapPath("~/UploadedResume/UploadedImages/"+pic_name));
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                        //Session["DeleteSuccess"] = "Yes";
                    }
                

                    string imageName = Guid.NewGuid() + Path.GetExtension(image.FileName);
                    image.SaveAs(Path.Combine(Server.MapPath("~/UploadedResume/UploadedImages"), imageName));


                    user_details.profilePic = imageName;

                }




                //userModal.userId = user_details.userId;
                user_details.firstName = orgModal.personName;
                    user_details.lastName = userModal.lastName;
                    user_details.password = userModal.password;
                    user_details.lastName = testcollection["lastName"];
                    //user_details.email = orgModal.email;
                    //userModal.email = orgModal.email; //email should not be editable
                    //user_details.catId = orgModal.catId;
                    //user_details.roleId = userModal.roleId;
                    //user_details.orgId = orgModal.orgId;
                    var active_state = testcollection["isactive"];
                    if (active_state == "on")
                    {
                        user_details.isActive = true;
                    }
                    else
                    {
                        user_details.isActive = false;
                    }

                    //userModal.createdDate = user_details.createdDate;
                    user_details.modifiedDate = orgModal.modifiedDate;
                    //userModal.profilePic = 
                    db1.Entry(user_details).State = System.Data.Entity.EntityState.Modified;
                    db1.SaveChanges();
                
            }
            if(testcollection["role"] == "admin")
            {
                return RedirectToAction("AdminProfile", new RouteValueDictionary(
                        new { controller = "Coordinator", action = "AdminProfile", Id = userModal.userId }));
            }
            else
            {
                return RedirectToAction("ViewOrg", "Admin");
            }
                
        }


        // END OF ORGANIZATION SECTION------------
        [Authorize]
        public ActionResult ViewOrg()
        {

            using (LearningDBEntities org = new LearningDBEntities())
            {
                var org_cat_id = (from a in org.Categories
                                   where a.name == "Organization"
                                   select a.catId).FirstOrDefault();
                
                ViewBag.OrgUserTable = emp.userTables.Where(a => a.catId == org_cat_id).ToList();
                //ViewBag.org = org.OrgTables.Where(a =>a.isActive == true).OrderByDescending(a => a.createdDate).ToList();
                ViewBag.org = org.OrgTables.OrderByDescending(a => a.createdDate).ToList();
                return View();
            }
        }

        
        public ActionResult DeleteOrg(string id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                ViewBag.org = emp.OrgTables.OrderByDescending(a => a.orgId).ToList();
                if (id != null)
                {
                    int org_Id = int.Parse(id);

                    OrgTable model = emp.OrgTables.Where(x => x.orgId == org_Id).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }

            }
        }
        [HttpPost]
        public ActionResult DeleteOrg(string id, OrgTable orgModel)
        {

            using (Models.LearningDBEntities dbModel = new Models.LearningDBEntities())
            {

                OrgTable emp = dbModel.OrgTables.Where(x => x.orgId == orgModel.orgId).FirstOrDefault();

                if (emp != null)
                {
                    dbModel.OrgTables.Remove(emp);
                    dbModel.SaveChanges();


                }

                return RedirectToAction("ViewOrg", "Admin");

            }


        }

        [Authorize]
        public ActionResult EditTrainer(int Id)
        {

            ViewBag.trainer = (from a in emp.userTables
                               where a.userId == Id
                               select a).Distinct().FirstOrDefault();
            return View();

        }


        // Delete Trainer

        public ActionResult DeleteTrainer(string id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                //to display list of trainers
                var category_id = (from a in emp.Categories
                                   where a.name == "trainer"
                                   select a.catId).FirstOrDefault();

                ViewBag.Trainers = emp.userTables.Where(a => a.catId == category_id).OrderByDescending(b => b.createdDate).ToList();
                //to display list of trainers---- end

                if (id != null)
                {
                    int TrainerId = int.Parse(id);

                    userTable model = emp.userTables.Where(x => x.userId == TrainerId).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }

            }
        }
        [HttpPost]
        public ActionResult DeleteTrainer(userTable userModal)
        {

            using (Models.LearningDBEntities dbModel = new Models.LearningDBEntities())
            {

                
                var user_details = (from a in dbModel.userTables
                                    where a.userId == userModal.userId
                                    select a).Distinct().FirstOrDefault();

                userTable emp = dbModel.userTables.Where(x => x.userId == userModal.userId).FirstOrDefault();
                
                var resume_name = user_details.resume;
                var path = Path.Combine(Server.MapPath("~/UploadedResume/" + resume_name));
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);

                    //Session["DeleteSuccess"] = "Yes";
                }

                if (emp != null)
                {
                    dbModel.userTables.Remove(emp);
                    dbModel.SaveChanges();


                }

                return RedirectToAction("ListTrainers", "User");

            }


        }

        [Authorize]
        public ActionResult EditStudentProfile(int Id)
        {

            ViewBag.student = (from a in emp.userTables
                               where a.userId == Id
                               select a).Distinct().FirstOrDefault();
            return View();

        }


        // Delete Student

        public ActionResult DeleteStudent(string id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                //to display list of trainers
                var category_id = (from a in emp.Categories
                                   where a.name == "student"
                                   select a.catId).FirstOrDefault();

                ViewBag.Students = emp.userTables.Where(a => a.catId == category_id).OrderByDescending(b => b.createdDate).ToList();
                //to display list of trainers---- end

                if (id != null)
                {
                    int StudentId = int.Parse(id);

                    userTable model = emp.userTables.Where(x => x.userId == StudentId).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }

            }
        }
        [HttpPost]
        public ActionResult DeleteStudent(string id, userTable userModel)
        {

            using (Models.LearningDBEntities dbModel = new Models.LearningDBEntities())
            {

                userTable emp = dbModel.userTables.Where(x => x.userId == userModel.userId).FirstOrDefault();

                if (emp != null)
                {
                    dbModel.userTables.Remove(emp);
                    dbModel.SaveChanges();
                }

                return RedirectToAction("ListStudents", "User");

            }


        }
        // ----------------------- ORGANIZATION ADMIN SECTION --- START---

        [Authorize]
        public ActionResult AdminViewCourse()
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                var org_id = (from a in emp.userTables
                              where a.email == User.Identity.Name
                              select a.orgId).Distinct().FirstOrDefault();

                if (org_id != null)
                {

                    ViewBag.crs = emp.CourseTables.Where(a => a.orgId == org_id).OrderByDescending(a => a.createdDate).ToList();
                    ViewBag.orgId = org_id;
                }
                return View();
            }


        }

    }
}