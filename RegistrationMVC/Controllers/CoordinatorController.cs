﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RegistrationMVC.Models;
using Newtonsoft.Json;
using System.IO;
using System.Web.Routing;

namespace RegistrationMVC.Controllers
{
    public class CoordinatorController : Controller
    {


        LearningDBEntities db = new LearningDBEntities();
        // GET: Coordinator

            [Authorize]
            public ActionResult ListCoordinators()
            {
                using (LearningDBEntities emp = new LearningDBEntities())
                {
                var RoleId = (from a in emp.RoleTables
                            where a.roleName == "Coordinator"
                            select a.roleId).FirstOrDefault();
                ViewBag.Coordinators = emp.userTables.Where(a => a.roleId == RoleId).OrderByDescending(b => b.createdDate).ToList();
                    return View();
                }
               
            
            }

    //---------------------- COURSES SECTION-------------------

        
       
     
       




        [HttpPost]
        public ActionResult SaveCourse(CourseTable courseModel,string chk,FormCollection frm)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {
                //if(chk == null)
                //{
                
                //}else
                //{
                //    courseModel.isActive = true;
                   
                //}
                if (frm["chk"] == null)
                {
                    courseModel.isActive = false;
                }
                else
                {
                    courseModel.isActive = true;
                }

                dbModel.Entry(courseModel).State = System.Data.Entity.EntityState.Modified;
                dbModel.SaveChanges();

                return RedirectToAction("ViewOrgCourse", new RouteValueDictionary(
                        new { controller = "Coordinator", action = "ViewOrgCourse", Id = courseModel.orgId }));

                //var db = dbModel.RegTables.Where(z => z.id == id && z.email == emp.email).FirstOrDefault();
                //if (db == null)
                //{
                //    ViewBag.message = "there is no data.";
                //}
                //else
                //{
                //    dbModel.Entry(emp).State = System.Data.Entity.EntityState.Modified;
                //    dbModel.SaveChanges();
                //    ViewBag.message = "successfully edited.";
                //}


            }



        }
        public ActionResult DeleteCourse(string id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                ViewBag.crs = emp.CourseTables.OrderByDescending(a => a.courseId).ToList();
                if (id != null)
                {
                    int crsid = int.Parse(id);

                    CourseTable model = emp.CourseTables.Where(x => x.courseId == crsid).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }

            }
        }
        [HttpPost]
        public ActionResult DeleteCourse(string id, CourseTable courseModal)
        {

            using (Models.LearningDBEntities dbModel = new Models.LearningDBEntities())
            {

                CourseTable emp = dbModel.CourseTables.Where(x => x.courseId == courseModal.courseId).FirstOrDefault();
            
                if (emp != null)
                {
                    dbModel.CourseTables.Remove(emp);
                    dbModel.SaveChanges();
                   

                }
               
                var org_id = (from a in dbModel.CourseTables
                              where a.courseId == courseModal.courseId
                              select a.orgId).Distinct().FirstOrDefault();
                return RedirectToAction("ViewOrgCourse", new RouteValueDictionary(
                         new { controller = "Coordinator", action = "ViewOrgCourse", Id = courseModal.orgId }));

            }


        }
        //---------------------- end of COURSES SECTION-------------------




        //---------------------- BATCH SECTION-------------------


        //for displaying batches..


        [Authorize]
        public ActionResult ViewBatch(string id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                ViewBag.batch = emp.BatchTables.OrderByDescending(a => a.batchId).ToList();
                if (id != null)
                {
                    int batId = int.Parse(id);

                    BatchTable model = emp.BatchTables.Where(x => x.batchId == batId).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }

            }
        }


        [HttpPost] //for Adding Batches
        public ActionResult AddBatch(BatchTable batchModel, FormCollection testcollection)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {

                if (dbModel.BatchTables.Any(x => x.batchName == batchModel.batchName))
                {
                    ViewBag.DuplicateMessage = "Batch with this name already exists.";
                    return RedirectToAction("ViewBatch", "Coordinator");
                }
                else
                {

                   
                    batchModel.startCourse = Convert.ToDateTime(testcollection["startdate"]);
                    batchModel.endCourse = Convert.ToDateTime(testcollection["enddate"]);
                    batchModel.createdDate = DateTime.Now;
                    batchModel.modifiedDate = DateTime.Now;
                    if (testcollection["isactive"] == "on")
                    {
                        batchModel.isActive = true;
                    }
                    else
                    {
                        batchModel.isActive = false;
                    }

                    dbModel.BatchTables.Add(batchModel);
                    dbModel.SaveChanges();
                    ModelState.Clear();
                    ViewBag.SuccessMessage = "Registration Successful";

                    return RedirectToAction("ViewBatch", "Coordinator");
                }

            }

        }



        [HttpPost]
        public ActionResult SaveBatch(BatchTable batchModel, string chk, FormCollection frm)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {
                if (frm["chk"] == null)
                {
                    batchModel.isActive = false;
                }
                else
                {
                    batchModel.isActive = true;
                }

                dbModel.Entry(batchModel).State = System.Data.Entity.EntityState.Modified;
                dbModel.SaveChanges();
                ViewBag.message = " Successful";
                return RedirectToAction("ViewBatch", "Coordinator");

                //var db = dbModel.RegTables.Where(z => z.id == id && z.email == emp.email).FirstOrDefault();
                //if (db == null)
                //{
                //    ViewBag.message = "there is no data.";
                //}
                //else
                //{
                //    dbModel.Entry(emp).State = System.Data.Entity.EntityState.Modified;
                //    dbModel.SaveChanges();
                //    ViewBag.message = "successfully edited.";
                //}


            }



        }






        public ActionResult DeleteBatch(string id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                ViewBag.batch = emp.BatchTables.OrderByDescending(a => a.batchId).ToList();
                if (id != null)
                {
                    int batId = int.Parse(id);

                    BatchTable model = emp.BatchTables.Where(x => x.batchId == batId).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }

            }
        }
        [HttpPost]
        public ActionResult DeleteBatch(string id, BatchTable batchModel)
        {

            using (Models.LearningDBEntities dbModel = new Models.LearningDBEntities())
            {

                BatchTable emp = dbModel.BatchTables.Where(x => x.batchId == batchModel.batchId).FirstOrDefault();

                if (emp != null)
                {
                    dbModel.BatchTables.Remove(emp);
                    dbModel.SaveChanges();


                }

                return RedirectToAction("ViewBatch", "Coordinator");

            }


        }


        //end of BATCH SECTION -------------------------


        [Authorize]
        public ActionResult AddCoordinator()
        {
            using (LearningDBEntities db = new LearningDBEntities())
            {
                ViewBag.organizations = (from a in db.OrgTables
                                         where a.coordinatorId == null
                                         select a).ToList();
                return View();
            }

               
        }

        //for Adding Coordinator from inside
        [HttpPost]
        public ActionResult AddCoordinator(userTable userModal, FormCollection testcollection, HttpPostedFileBase image)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {

                if (dbModel.userTables.Any(x => x.email == userModal.email))
                {
                    ViewBag.DuplicateMessage = "Coordinator with this email already exists.";

                    return View("AddCoordinator", "Coordinator");
                }
                else
                {

                    if (image == null)
                    {
                        userModal.profilePic = null;
                    }
                    else
                    {
                        string imageName = Guid.NewGuid() + Path.GetExtension(image.FileName);
                        image.SaveAs(Path.Combine(Server.MapPath("~/UploadedResume/UploadedImages"), imageName));


                        userModal.profilePic = imageName;

                    }
                     Guid guid = Guid.NewGuid();
                    Random random = new Random();
                    int key = random.Next();

                    userModal.userId = key;
                    userModal.firstName = testcollection["fname"];
                    userModal.lastName = testcollection["lname"];
                    userModal.password = testcollection["pass"];
                    userModal.email = testcollection["email"];


                    // for adding in userTable and orgTable
                    

                    var org_name_ids = testcollection["orgName"];
                    if(org_name_ids != null)
                    {
                        userModal.co_org_Id = org_name_ids;

                        var someString = org_name_ids;
                        var results = someString.Split(',');
                        foreach (var item in results)
                        {


                            var org_id = int.Parse(item);
                            var org_details = (from a in dbModel.OrgTables
                                               where a.orgId == org_id
                                               select a).Distinct().FirstOrDefault();
                            org_details.coordinatorId = key; // passing co-ordinator id(random) to orgTable's coordinatorId

                            dbModel.Entry(org_details).State = System.Data.Entity.EntityState.Modified;
                            dbModel.SaveChanges();


                        }
                    }

                    // ----------------for adding in userTable and orgTable
                    if(org_name_ids == null)
                    {
                        userModal.co_org_Id = null;
                    }
                   


                    var role_id = (from a in dbModel.RoleTables
                                   where a.roleName == "Coordinator"
                                   select a.roleId).Distinct().FirstOrDefault();

                    userModal.roleId = role_id;
                    
                    userModal.createdDate = DateTime.Now;
                    userModal.modifiedDate = DateTime.Now;
                    if (testcollection["isactive"] == "on")
                    {
                        userModal.isActive = true;
                    }
                    else
                    {
                        userModal.isActive = false;
                    }

                    dbModel.userTables.Add(userModal);
                    dbModel.SaveChanges();
                    ModelState.Clear();
                    ViewBag.SuccessMessage = "Registration Successful";

                    return RedirectToAction("ListCoordinators", "Coordinator");
                }

            }
        }



        [Authorize]
        public ActionResult EditCoordinator(int id)
        {

            ViewBag.coordinator = (from a in db.userTables
                                   where a.userId == id
                                   select a).Distinct().FirstOrDefault();
            //ViewBag.organizations = (from a in db.userTables
            //                         where a.userId == id
            //                         select a.co_org_Id).Distinct().FirstOrDefault();
            ViewBag.selectedOrg = (from a in db.OrgTables
                                     where a.coordinatorId == id
                                     select a).ToList();

            ViewBag.allOrg = (from a in db.OrgTables                              
                              select a).ToList();

            ViewBag.notSelectedOrg = (from a in db.OrgTables
                                      where a.coordinatorId == null
                                      select a).ToList();

            return View();

        }

        //[HttpPost]
        //public ActionResult SaveCoordinatorProfile(int id)
        //{
        //    return RedirectToAction("CoordinatorProfile", new RouteValueDictionary(
        //                new { controller = "Coordinator", action = "CoordinatorProfile", Id = id }));
        //}


        [HttpPost]
        public ActionResult SaveCoordinator(userTable userModal, FormCollection testcollection, HttpPostedFileBase image)
        {
           

            using (LearningDBEntities db1 = new LearningDBEntities())
            {
                // for adding in userTable
                var user_details = (from a in db1.userTables
                                    where a.userId == userModal.userId
                                    select a).Distinct().FirstOrDefault();
               
                //for editing profile pics

                if (image == null)
                {
                    var pic = user_details.profilePic;

                    user_details.profilePic = pic;


                }
                if (image != null)
                {
                    var pic_name = user_details.profilePic;
                    var path = Path.Combine(Server.MapPath("~/UploadedResume/UploadedImages/" + pic_name));
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                        //Session["DeleteSuccess"] = "Yes";
                    }


                    string imageName = Guid.NewGuid() + Path.GetExtension(image.FileName);
                    image.SaveAs(Path.Combine(Server.MapPath("~/UploadedResume/UploadedImages"), imageName));


                    user_details.profilePic = imageName;

                }




                    //userModal.userId = user_details.userId;
                    user_details.firstName = userModal.firstName;
                    user_details.lastName = userModal.lastName;
                    user_details.password = userModal.password;
                    
                    var active_state = testcollection["isactive"];
                    if (active_state == "on")
                    {
                        user_details.isActive = true;
                    }
                    else
                    {
                        user_details.isActive = false;
                    }

                
                


                var org_name_ids = testcollection["orgName"];

                

                
                user_details.co_org_Id = org_name_ids;


                user_details.modifiedDate = userModal.modifiedDate;
                db1.Entry(user_details).State = System.Data.Entity.EntityState.Modified;
                db1.SaveChanges();

                // -- end of adding data in userTable


                var org_ids2 = (from a in db1.OrgTables
                                where a.coordinatorId == user_details.userId
                                select a.orgId).ToList(); // orgs which were under this coordinator in OrgTable


                
                if (org_name_ids == null) //when selections is null
                {
                    if (org_ids2.Count() != 0) // this for: when entering data for first time.
                    {
                        foreach (var item in org_ids2) // to remove else data
                        {

                            var org_details = (from a in db1.OrgTables
                                               where a.orgId == item
                                               select a).Distinct().FirstOrDefault();
                            org_details.coordinatorId = null;// passing null to orgTable's coordinatorId

                            db1.Entry(org_details).State = System.Data.Entity.EntityState.Modified;
                            db1.SaveChanges();
                        }
                    }


                }
                

                if (org_name_ids != null)
                {
                    var someString = org_name_ids;
                    var results = someString.Split(',');

                    //if no orgs are there for particular coordinator
                    if (org_ids2.Count() == 0) // this for: when entering data for first time.
                    {
                        foreach (var item in results)
                        {
                            var org_id = int.Parse(item);


                            var org_details = (from a in db1.OrgTables
                                               where a.orgId == org_id
                                               select a).Distinct().FirstOrDefault();
                            org_details.coordinatorId = userModal.userId; // passing co-ordinator id(random) to orgTable's coordinatorId

                            db1.Entry(org_details).State = System.Data.Entity.EntityState.Modified;
                            db1.SaveChanges();
                        }
                    }
                    else // if there are organizations under this coordinator
                    {

                        foreach (var item2 in org_ids2) // to replace all previous existing co_ordId's to "null" so that to include new ones.
                        {
                            
                            var org_id = item2;

                            var org_details = (from a in db1.OrgTables
                                               where a.orgId == org_id
                                               select a).Distinct().FirstOrDefault();
                            org_details.coordinatorId = null;
                            db1.Entry(org_details).State = System.Data.Entity.EntityState.Modified;
                            db1.SaveChanges();
                        }
                        //traced till here...

                        foreach (var item in results) // to enter particular coordIds' into org table
                        {

                            var org_id = int.Parse(item);


                            var org_details = (from a in db1.OrgTables
                                               where a.orgId == org_id
                                               select a).Distinct().FirstOrDefault();
                            org_details.coordinatorId = userModal.userId; // passing co-ordinator id(random) to orgTable's coordinatorId

                            db1.Entry(org_details).State = System.Data.Entity.EntityState.Modified;
                            db1.SaveChanges();



                        }
                    }

                   
                }
             


                
                
                
            }
            return RedirectToAction("ListCoordinators", "Coordinator");
        }

        [Authorize]
        public ActionResult CoordinatorProfile(int Id)
        {

          //  var org_name_ids = (from a in db.userTables
          //                      where a.userId == Id
          //                      select a.co_org_Id).Distinct().FirstOrDefault();

          //  var someString = org_name_ids;
          //  List<string> result = someString.Split(',').ToList();
            
          // ViewBag.ids = result;
          ////  var results = someString.Split(',');
          

            ViewBag.organizations = (from a in db.OrgTables
                                   
                                     select a).ToList();
            ViewBag.coordinator = (from a in db.userTables
                                   where a.userId == Id
                                   select a).Distinct().FirstOrDefault();
            return View();
        }


        [Authorize]
        public ActionResult CoOrgView(int id)
        {

            using (LearningDBEntities org = new LearningDBEntities())
            {
                var org_cat_id = (from a in org.Categories
                                  where a.name == "Organization"
                                  select a.catId).FirstOrDefault();


                //ViewBag.OrgUserTable = org.userTables.Where(a => a.catId == org_cat_id).ToList();

                //ViewBag.org = org.OrgTables.OrderByDescending(a => a.createdDate).ToList();



                //test
                var org_name_ids = (from a in db.userTables
                                    where a.userId == id
                                    select a.co_org_Id).Distinct().FirstOrDefault();

                var someString = org_name_ids;
                if(someString == null)
                {
                    ViewBag.ids = null;
                }
                else
                {
                    List<string> result = someString.Split(',').ToList();

                    ViewBag.ids = result;
                }
                
                ViewBag.OrgUserTable = org.userTables.Where(a => a.catId == org_cat_id).ToList();
                ViewBag.org = (from a in db.OrgTables
                                         select a).ToList();

                


                return View();
            }
        }


        [Authorize]
        public ActionResult AdminProfile(int Id)
        {

            using (LearningDBEntities db = new LearningDBEntities())
            {
                // for taking org Id to display courses
                ViewBag.org = (from a in db.userTables
                                where a.email == User.Identity.Name
                                select a.orgId).Distinct().FirstOrDefault();
                //-----------

                var org_id = (from a in db.userTables
                               where a.userId == Id
                               select a.orgId).Distinct().FirstOrDefault();
                ViewBag.org = (from a in db.OrgTables
                               where a.orgId == org_id
                               select a).Distinct().FirstOrDefault();
                ViewBag.user = (from a in db.userTables
                                where a.userId == Id
                                select a).Distinct().FirstOrDefault();

            }

            return View();
        }



        // for Viewing course cards
        [Authorize]
        public ActionResult ViewOrgCourse(string id, CourseTable crs)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                //var org_id = (from a in emp.userTables
                //              where a.email == User.Identity.Name
                //              select a.orgId).Distinct().FirstOrDefault();

                if(id != null)
                {
                    var id1 = int.Parse(id);
                    ViewBag.crs = emp.CourseTables.Where(a => a.orgId == id1).OrderByDescending(a => a.courseId).ToList();
                    ViewBag.orgId = id;
                }
                return View();
                //if (id != null)
                //{

                //    int crsid = int.Parse(id);
                //    crs.isActive = (from a in emp.CourseTables
                //                    where a.courseId == crsid
                //                    select a.isActive).Distinct().FirstOrDefault();

                //    CourseTable model = emp.CourseTables.Where(x => x.courseId == crsid).FirstOrDefault();
                //    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                //    {
                //        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                //    });
                //    return Json(value, JsonRequestBehavior.AllowGet);

                //}
                //else
                //{

                //    return View();
                //}





            }
        }



        [HttpPost] //for Adding Course
        public ActionResult AddCourse(int id, CourseTable courseModel, FormCollection testcollection)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {

                //if (dbModel.CourseTables.Any(x => x.CourseName == courseModel.CourseName))
                //{
                //    ViewBag.DuplicateMessage = "Course with this name already exists.";
                //    return RedirectToAction("ViewCourse", "User");
                //}
                var isexist = (from a in dbModel.CourseTables
                               where a.orgId == id && a.CourseName == courseModel.CourseName
                               select a.courseId).Distinct().FirstOrDefault();
                if(isexist != 0)
                {
                    ViewBag.DuplicateMessage = "Course with this name already exists.";
                      return RedirectToAction("ViewOrgCourse", "Coordinator");
                }
                else
                {


                    courseModel.duration = testcollection["duration"];
                    courseModel.amount = testcollection["amount"];
                    courseModel.createdDate = DateTime.Now;
                    courseModel.modifiedDate = DateTime.Now;
                    if (testcollection["isactive"] == "on")
                    {
                        courseModel.isActive = true;
                    }
                    else
                    {
                        courseModel.isActive = false;
                    }
                    //var org_id = (from a in dbModel.userTables
                    //              where a.email == User.Identity.Name
                    //              select a.orgId).Distinct().FirstOrDefault();
                    courseModel.orgId = id;
                    dbModel.CourseTables.Add(courseModel);
                    dbModel.SaveChanges();
                    ModelState.Clear();


                    return RedirectToAction("ViewOrgCourse", new RouteValueDictionary(
                        new { controller = "Coordinator", action = "ViewOrgCourse", id = courseModel.orgId }));
                }

            }

        }


        [Authorize] // for editing course
        public ActionResult EditOrgCourse(string id, CourseTable crs)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {


                if (id != null)
                {

                    int crsid = int.Parse(id);
                    crs.isActive = (from a in emp.CourseTables
                                    where a.courseId == crsid
                                    select a.isActive).Distinct().FirstOrDefault();

                    CourseTable model = emp.CourseTables.Where(x => x.courseId == crsid).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }





            }
        }


        //----------------------ORG BATCH SECTION-------------------


        //for displaying batches..


        [Authorize]
        public ActionResult ViewOrgBatch(string id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                var org_id = (from a in emp.userTables
                              where a.email == User.Identity.Name
                              select a.orgId).Distinct().FirstOrDefault();
                if (org_id != null)
                {
                    ViewBag.batch = emp.BatchTables.Where(a => a.orgId == org_id).OrderByDescending(a => a.batchId).ToList();
                }
              
                if (id != null)
                {
                    int batId = int.Parse(id);

                    BatchTable model = emp.BatchTables.Where(x => x.batchId == batId).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }

            }
        }



        


        [HttpPost] //for Adding Batches
        public ActionResult AddOrgBatch(BatchTable batchModel, FormCollection testcollection)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {
                var isexist = (from a in dbModel.BatchTables
                               where a.orgId == batchModel.orgId && a.batchName == batchModel.batchName
                               select a.batchId).Distinct().FirstOrDefault();
                if (isexist != 0)
                {
                    ViewBag.DuplicateMessage = "Batch with this name already exists.";
                    return RedirectToAction("ViewOrgBatch", "Coordinator");
                }
                
                else
                {


                    batchModel.startCourse = Convert.ToDateTime(testcollection["startdate"]);
                    batchModel.endCourse = Convert.ToDateTime(testcollection["enddate"]);
                    batchModel.createdDate = DateTime.Now;
                    batchModel.modifiedDate = DateTime.Now;
                    if (testcollection["isactive"] == "on")
                    {
                        batchModel.isActive = true;
                    }
                    else
                    {
                        batchModel.isActive = false;
                    }
                    var org_id = (from a in dbModel.userTables
                                  where a.email == User.Identity.Name
                                  select a.orgId).Distinct().FirstOrDefault();
                    batchModel.orgId = org_id;
                    dbModel.BatchTables.Add(batchModel);
                    dbModel.SaveChanges();
                    ModelState.Clear();
                   

                    return RedirectToAction("ViewOrgBatch", "Coordinator");
                }

            }

        }



        [HttpPost]
        public ActionResult SaveOrgBatch(BatchTable batchModel, string chk, FormCollection frm)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {
                if (frm["chk"] == null)
                {
                    batchModel.isActive = false;
                }
                else
                {
                    batchModel.isActive = true;
                }

                dbModel.Entry(batchModel).State = System.Data.Entity.EntityState.Modified;
                dbModel.SaveChanges();
                ViewBag.message = " Successful";
                return RedirectToAction("ViewOrgBatch", "Coordinator");

                


            }



        }

        public ActionResult DeleteOrgBatch(string id)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                ViewBag.batch = emp.BatchTables.OrderByDescending(a => a.batchId).ToList();
                if (id != null)
                {
                    int batId = int.Parse(id);

                    BatchTable model = emp.BatchTables.Where(x => x.batchId == batId).FirstOrDefault();
                    var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);

                }
                else
                {

                    return View();
                }

            }
        }
        [HttpPost]
        public ActionResult DeleteOrgBatch(string id, BatchTable batchModel)
        {

            using (Models.LearningDBEntities dbModel = new Models.LearningDBEntities())
            {

                BatchTable emp = dbModel.BatchTables.Where(x => x.batchId == batchModel.batchId).FirstOrDefault();

                if (emp != null)
                {
                    dbModel.BatchTables.Remove(emp);
                    dbModel.SaveChanges();


                }

                return RedirectToAction("ViewOrgBatch", "Coordinator");

            }


        }






    }
}