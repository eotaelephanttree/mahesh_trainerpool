﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RegistrationMVC.Models;
using Newtonsoft.Json;
using System.IO;
using System.Web.Routing;

namespace RegistrationMVC.Controllers
{
    public class StudentController : Controller
    {
        LearningDBEntities db = new LearningDBEntities();



        [Authorize]
        public ActionResult StudentProfile(int Id)
        {
            
            ViewBag.student = (from a in db.userTables
                                   where a.userId == Id
                                   select a).Distinct().FirstOrDefault();
            return View();
            
        }


        [HttpPost]
        public ActionResult SaveStudent(int id, userTable userModal, FormCollection testcollection, HttpPostedFileBase image)
        {
            using (LearningDBEntities db = new LearningDBEntities())
            {
                // retrieving current details
                var user_details = (from a in db.userTables
                                    where a.userId == id
                                    select a).Distinct().FirstOrDefault();

                //for editing profile pics

                if (image == null)
                {
                    var pic = user_details.profilePic;

                    user_details.profilePic = pic;


                }
                if (image != null)
                {
                    var pic_name = user_details.profilePic;
                    var path = Path.Combine(Server.MapPath("~/UploadedResume/UploadedImages/" + pic_name));
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                        //Session["DeleteSuccess"] = "Yes";
                    }


                    string imageName = Guid.NewGuid() + Path.GetExtension(image.FileName);
                    image.SaveAs(Path.Combine(Server.MapPath("~/UploadedResume/UploadedImages"), imageName));


                    user_details.profilePic = imageName;

                }




                //userModal.userId = user_details.userId;
                user_details.firstName = userModal.firstName;
                user_details.lastName = userModal.lastName;
                user_details.password = userModal.password;

                var active_state = testcollection["isactive"];
                if (active_state == "on")
                {
                    user_details.isActive = true;
                }
                else
                {
                    user_details.isActive = false;
                }

                user_details.modifiedDate = userModal.modifiedDate;
                db.Entry(user_details).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

            }
            return RedirectToAction("StudentProfile", new RouteValueDictionary(
                     new { controller = "Student", action = "StudentProfile", Id = id }));
        }


        

    }
}