﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RegistrationMVC.Models;

namespace RegistrationMVC.Controllers
{
    public class HomeController : Controller
    {
        


        public ActionResult HomePage()
        {
            using (LearningDBEntities org = new LearningDBEntities())
            {
                ViewBag.cat = org.Categories.OrderByDescending(a => a.catId).ToList(); //dropdown for category
                return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}