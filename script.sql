USE [master]
GO
/****** Object:  Database [LearningDB]    Script Date: 5/17/2018 3:46:25 PM ******/
CREATE DATABASE [LearningDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LearningDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\LearningDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LearningDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\LearningDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [LearningDB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LearningDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LearningDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LearningDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LearningDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LearningDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LearningDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [LearningDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LearningDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LearningDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LearningDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LearningDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LearningDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LearningDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LearningDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LearningDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LearningDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LearningDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LearningDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LearningDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LearningDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LearningDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LearningDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LearningDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LearningDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LearningDB] SET  MULTI_USER 
GO
ALTER DATABASE [LearningDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LearningDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LearningDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LearningDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LearningDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LearningDB] SET QUERY_STORE = OFF
GO
USE [LearningDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [LearningDB]
GO
/****** Object:  Table [dbo].[BatchTable]    Script Date: 5/17/2018 3:46:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BatchTable](
	[batchId] [int] IDENTITY(1,1) NOT NULL,
	[batchName] [nvarchar](50) NULL,
	[startCourse] [date] NULL,
	[endCourse] [date] NULL,
	[isActive] [bit] NULL,
	[createdDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
	[orgId] [int] NULL,
 CONSTRAINT [PK_BatchTable] PRIMARY KEY CLUSTERED 
(
	[batchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/17/2018 3:46:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[catId] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[isActive] [bit] NOT NULL,
	[createdDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[catId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CoorgTable]    Script Date: 5/17/2018 3:46:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CoorgTable](
	[orgId] [int] NOT NULL,
	[orgName] [nvarchar](max) NULL,
	[coordId] [int] NULL,
	[coordName] [nvarchar](max) NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_CoorgTable] PRIMARY KEY CLUSTERED 
(
	[orgId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CourseTable]    Script Date: 5/17/2018 3:46:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CourseTable](
	[courseId] [int] IDENTITY(1,1) NOT NULL,
	[CourseName] [nvarchar](50) NULL,
	[duration] [nvarchar](50) NULL,
	[isActive] [bit] NOT NULL,
	[createdDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
	[orgId] [int] NULL,
	[amount] [nvarchar](50) NULL,
 CONSTRAINT [PK_CourseTable] PRIMARY KEY CLUSTERED 
(
	[courseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[imageTable]    Script Date: 5/17/2018 3:46:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[imageTable](
	[imageId] [int] NOT NULL,
	[orgId] [int] NULL,
	[userId] [int] NULL,
	[name] [nvarchar](128) NULL,
	[type] [nvarchar](128) NULL,
	[size] [bigint] NULL,
	[path] [nvarchar](512) NULL,
	[category] [nvarchar](24) NULL,
	[isDefault] [int] NULL,
	[isActive] [bit] NULL,
	[modifiedDate] [datetime] NULL,
	[createdDate] [datetime] NULL,
 CONSTRAINT [PK_imageTable] PRIMARY KEY CLUSTERED 
(
	[imageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrgTable]    Script Date: 5/17/2018 3:46:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrgTable](
	[orgId] [int] NOT NULL,
	[catId] [int] NULL,
	[orgName] [nvarchar](64) NULL,
	[personName] [nvarchar](64) NULL,
	[mobile] [nvarchar](64) NULL,
	[phone] [nvarchar](64) NULL,
	[email] [nvarchar](64) NULL,
	[amount] [int] NULL,
	[isActive] [bit] NOT NULL,
	[createdDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
	[coordinatorId] [int] NULL,
	[trainerId] [nvarchar](max) NULL,
 CONSTRAINT [PK_OrgTable] PRIMARY KEY CLUSTERED 
(
	[orgId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RegTable]    Script Date: 5/17/2018 3:46:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RegTable](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fname] [nvarchar](50) NULL,
	[lname] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[email] [nvarchar](max) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[isAdmin] [bit] NOT NULL,
	[createdDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
 CONSTRAINT [PK_RegTable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleTable]    Script Date: 5/17/2018 3:46:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleTable](
	[roleId] [int] NOT NULL,
	[roleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_RoleTable] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SkillsTable]    Script Date: 5/17/2018 3:46:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SkillsTable](
	[TrainerId] [int] NOT NULL,
	[skills] [nvarchar](max) NULL,
	[experience] [nvarchar](50) NULL,
 CONSTRAINT [PK_SkillsTable] PRIMARY KEY CLUSTERED 
(
	[TrainerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[userTable]    Script Date: 5/17/2018 3:46:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userTable](
	[userId] [int] NOT NULL,
	[userName] [nvarchar](max) NULL,
	[firstName] [nvarchar](max) NULL,
	[lastName] [nvarchar](max) NULL,
	[password] [nvarchar](max) NULL,
	[email] [nvarchar](max) NULL,
	[catId] [int] NULL,
	[roleId] [int] NULL,
	[orgId] [int] NULL,
	[isAdmin] [bit] NULL,
	[isActive] [bit] NULL,
	[createdDate] [datetime] NULL,
	[modifiedDate] [datetime] NULL,
	[resume] [nvarchar](max) NULL,
	[profilePic] [nvarchar](max) NULL,
	[co_org_Id] [nvarchar](max) NULL,
 CONSTRAINT [PK_userTable] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [LearningDB] SET  READ_WRITE 
GO
