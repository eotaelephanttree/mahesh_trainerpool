﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Newtonsoft.Json;
using RegistrationMVC.Models;


namespace RegistrationMVC.Controllers
{
    public class UserController : Controller
    {
        

        

        [HttpPost]
        
        public ActionResult Login(userTable userModel, FormCollection testcollection, string ReturnUrl="")
            {
            using (LearningDBEntities db = new LearningDBEntities())
            {
                var e_mail = testcollection["email"];
                var pword = testcollection["password"];
                var userDetails = db.userTables.Where(x => x.email == e_mail && x.password == pword).FirstOrDefault();
                if (userDetails == null)
                {
                    ViewBag.LoginErrorMessage = "Wrong username or password.";
                    return View("HomePage", "Home");
                }
                else
                {


                    //int timeout = regModel.rememberMe ? 525600 : 1;
                    int timeout = 60*24;
                    //var ticket = new FormsAuthenticationTicket(userModel.email, userModel.rememberMe, timeout);
                    var ticket = new FormsAuthenticationTicket(userModel.email, userModel.rememberMe, timeout);
                    string encrypted = FormsAuthentication.Encrypt(ticket);
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                    //cookie.Expires = DateTime.Now.AddMinutes(timeout);
                    cookie.Expires = DateTime.Now.AddMinutes(timeout);
                    cookie.HttpOnly = true;
                    Response.Cookies.Add(cookie);
                    ViewBag.name = (from a in db.userTables
                                    where a.email==User.Identity.Name
                                    select a.firstName).Distinct().FirstOrDefault();
                    ViewBag.Success = "Login Successful";
                    Session["userId"] = userDetails.userId;
                    Session["firstName"] = userDetails.firstName;
                    Session["profilePic"] = userDetails.profilePic;
                    

                    var role = (from a in db.RoleTables
                                       where a.roleId == userDetails.roleId
                                       select a.roleName).FirstOrDefault();

                    if (role == "Student") // for Student login
                    {
                        return RedirectToAction("StudentProfile", new RouteValueDictionary(
                         new { controller = "Student", action = "StudentProfile", Id = userDetails.userId }));
                    }
                    if (role=="SuperAdmin")
                    {
                        return RedirectToAction("MyProfile", "Admin");
                    }
                    if (role == "Coordinator")
                    {
                        return RedirectToAction("CoordinatorProfile", new RouteValueDictionary(
                        new { controller = "Coordinator", action = "CoordinatorProfile", Id = userDetails.userId }));
                    }
                    if(role == "Admin")
                    {
                        
                        return RedirectToAction("AdminProfile", new RouteValueDictionary(
                        new { controller = "Coordinator", action = "AdminProfile", Id = userDetails.userId }));
                    }
                    else
                    {
                        return RedirectToAction("MyProfile", "Admin");
                    }
                    
                    //return RedirectToAction("MyProfile", new RouteValueDictionary(
                    //new { controller = "Admin", action = "MyProfile", Id = userDetails.id }));

                }

            }

        }


        public ActionResult Login()
        {
            
            return View();
        }

        

       

        
        [HttpPost]
        public ActionResult Logout()
        {
            Session.Abandon();
            Session.Clear();
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            
            return RedirectToAction("HomePage", "Home"); //(Action/View, Controller)
        }

        
        
        

        public ActionResult Registration_test()
        {
            return View();
        }

        public ActionResult SignUp()
        {
            return View();
        }

        [Authorize]
        public ActionResult TrainerPage()
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                ViewBag.organizations = (from a in emp.OrgTables
                                         
                                         select a).ToList();

                return View(emp.RegTables.ToList());
            }
           
        }

        // for Viewing course cards and getting into edit modal
        [Authorize]
        public ActionResult ViewCourse(string id,CourseTable crs)
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                ViewBag.crs = emp.CourseTables.OrderByDescending(a=>a.courseId).ToList();
                if (id!=null)
                {
                    
                    int crsid = int.Parse(id);
                    crs.isActive = (from a in emp.CourseTables
                                    where a.courseId == crsid
                                    select a.isActive).Distinct().FirstOrDefault();
                   
                    CourseTable model = emp.CourseTables.Where(x => x.courseId == crsid).FirstOrDefault();
                   var value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    return Json(value, JsonRequestBehavior.AllowGet);
                    
                }else
                {
                    
                    return View();
                }
               
            }
        }


        [HttpPost] //for Adding Course
        public ActionResult AddCourse(CourseTable courseModel, FormCollection testcollection)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {

                if (dbModel.CourseTables.Any(x => x.CourseName == courseModel.CourseName))
                {
                    ViewBag.DuplicateMessage = "Course with this name already exists.";
                    return RedirectToAction("ViewCourse", "User");
                }
                 else
                {

                    
                    courseModel.duration = testcollection["duration"];
                    courseModel.amount = testcollection["amount"];
                    courseModel.createdDate = DateTime.Now;
                    courseModel.modifiedDate = DateTime.Now;
                    if (testcollection["isactive"] == "on")
                    {
                        courseModel.isActive = true;
                    }
                    else
                    {
                        courseModel.isActive = false;
                    }
                   
                    dbModel.CourseTables.Add(courseModel);
                    dbModel.SaveChanges();
                    ModelState.Clear();
                    ViewBag.SuccessMessage = "Registration Successful";
                    
                    return RedirectToAction("ViewCourse", "User");
                }

            }

        }



        [Authorize]
        public ActionResult AddStudent()
        {
            return View();
        }




        //for Adding student from registration page
        [HttpPost]
        public ActionResult AddStudent(userTable userModal, FormCollection testcollection, HttpPostedFileBase image)
        {
            using (LearningDBEntities dbModel = new LearningDBEntities())
            {

                if (dbModel.userTables.Any(x => x.email == userModal.email))
                {
                    ViewBag.DuplicateMessage = "Student with this email already exists.";

                    return View("HomePage", "Home");
                }
                else
                {

                    if (image == null)
                    {
                        userModal.profilePic = null;
                    }
                    else
                    {
                        string imageName = Guid.NewGuid() + Path.GetExtension(image.FileName);
                        image.SaveAs(Path.Combine(Server.MapPath("~/UploadedResume/UploadedImages"), imageName));


                        userModal.profilePic = imageName;

                    }





                    Guid guid = Guid.NewGuid();
                    Random random = new Random();
                    int key = random.Next();

                    userModal.userId = key;
                    userModal.firstName = testcollection["fname"];
                    userModal.lastName = testcollection["lname"];
                    userModal.password = testcollection["pass"];
                    userModal.email = testcollection["email"];


                    var category_id = (from a in dbModel.Categories
                                       where a.name == "student"
                                       select a.catId).FirstOrDefault();

                    userModal.catId = category_id;
                    userModal.createdDate = DateTime.Now;
                    userModal.modifiedDate = DateTime.Now;
                    if (testcollection["isactive"] == "on")
                    {
                        userModal.isActive = true;
                    }
                    else
                    {
                        userModal.isActive = false;
                    }
                   
                    dbModel.userTables.Add(userModal);
                    dbModel.SaveChanges();
                    ModelState.Clear();
                    ViewBag.SuccessMessage = "Registration Successful";

                   
                    return RedirectToAction("HomePage", "Home");
                }

            }
        }





      

        
        
        // Adding Trainer from inside and registration page
        public ActionResult AddTrainer(userTable userModal, FormCollection testcollection, HttpPostedFileBase file, HttpPostedFileBase image)
        {

            using (LearningDBEntities dbModel = new LearningDBEntities())
            {

                if (dbModel.userTables.Any(x => x.email == userModal.email))
                {
                    ViewBag.DuplicateMessage = "Trainer with this email already exists.";

                    return View("TrainerPage", "User");
                }
                else
                {
                    //if (file == null)
                    //{
                    //    ModelState.AddModelError("CustomError", "Please select CV");
                    //    return View();
                    //}

                    //if (!(file.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                    //    file.ContentType == "application/pdf"))
                    //{
                    //    ModelState.AddModelError("CustomError", "Only .docx and .pdf file allowed");
                    //    return View();
                    //}

                    
                    

                        try
                        {
                       

                        if(file == null)
                        {
                            userModal.resume = null;
                        }
                        else
                        {
                            string fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                            file.SaveAs(Path.Combine(Server.MapPath("~/UploadedResume"), fileName));

                            userModal.resume = fileName;
                        }
                            



                        if(image == null)
                        {
                            userModal.profilePic = null;
                        }
                        else
                        {
                            string imageName = Guid.NewGuid() + Path.GetExtension(image.FileName);
                            image.SaveAs(Path.Combine(Server.MapPath("~/UploadedResume/UploadedImages"), imageName));


                            userModal.profilePic = imageName;

                        }
                        
                        
                               
                            
                            Guid guid = Guid.NewGuid();
                            Random random = new Random();
                            int key = random.Next();

                            userModal.userId = key;
                            userModal.firstName = testcollection["fname"];
                            userModal.lastName = testcollection["lname"];
                            userModal.password = testcollection["pass"];
                            userModal.email = testcollection["email"];

                        // for storing organization Id into trainer 
                        var org = testcollection["orgName"];
                        if(org == "null")
                        {
                            userModal.orgId = null;
                        }
                        else //and trainerId in org table
                        {
                            var org_id = int.Parse(org);
                            userModal.orgId = org_id;
                            var org_details = (from a in dbModel.OrgTables
                                               where a.orgId == org_id
                                               select a).Distinct().FirstOrDefault();

                            var present_trainer = org_details.trainerId;
                            if(present_trainer == null)
                            {

                                org_details.trainerId = key.ToString();
                                dbModel.Entry(org_details).State = System.Data.Entity.EntityState.Modified;
                                dbModel.SaveChanges();
                            }
                            else
                            {
                                org_details.trainerId = present_trainer + "," + key.ToString();
                                dbModel.Entry(org_details).State = System.Data.Entity.EntityState.Modified;
                                dbModel.SaveChanges();
                            }

                        }
                        // for storing organization Id into trainer(userTable) and trainerId in org table ------- END

                        var category_id = (from a in dbModel.Categories
                                               where a.name == "trainer"
                                               select a.catId).FirstOrDefault();
                        var role_id = (from a in dbModel.RoleTables
                                    where a.roleName == "trainer"
                                    select a.roleId).Distinct().FirstOrDefault();
                        userModal.roleId = role_id;

                            userModal.catId = category_id;
                            userModal.createdDate = DateTime.Now;
                            userModal.modifiedDate = DateTime.Now;
                            if (testcollection["isactive"] == "on")
                            {
                                userModal.isActive = true;
                            }
                            else
                            {
                                userModal.isActive = false;
                            }
                            userModal.userId = key;
                            dbModel.userTables.Add(userModal);
                            dbModel.SaveChanges();
                            ModelState.Clear();
                            ViewBag.SuccessMessage = "Registration Successful";
                          
                            return RedirectToAction("TrainerPage");
                           // ModelState.Clear();
                            //userModal = null;
                            //ViewBag.Message = "Successfully Done";
                        }
                        catch (Exception ex)
                        {
                            ViewBag.Message = "Error! Please try again";
                            return RedirectToAction("TrainerPage");
                        }
                   
                   
                    
                }
            }
        }


        [Authorize]
        public ActionResult ListTrainers()
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                var category_id = (from a in emp.Categories
                                   where a.name == "trainer"
                                   select a.catId).FirstOrDefault();
                
                ViewBag.Trainers = emp.userTables.Where(a => a.catId == category_id).OrderByDescending(b=>b.createdDate).ToList();
                return View();
            }
        }

        // Students section.....

        [Authorize]
        public ActionResult ListStudents()
        {
            using (LearningDBEntities emp = new LearningDBEntities())
            {
                var category_id = (from a in emp.Categories
                                   where a.name == "student"
                                   select a.catId).FirstOrDefault();

                ViewBag.Students = emp.userTables.Where(a => a.catId == category_id).OrderByDescending(b => b.createdDate).ToList();
                return View();
            }
        }

        


    }

        


    }

    


