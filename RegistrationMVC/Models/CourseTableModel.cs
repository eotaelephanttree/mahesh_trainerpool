﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegistrationMVC.Models
{
    public class CourseTableModel
    {
        public int courseId { get; set; }
        public string CourseName { get; set; }
        public string duration { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> modifiedDate { get; set; }
        public Nullable<int> orgId { get; set; }
        public string amount { get; set; }
    }
}